const express = require('express');
const router = express.Router();

//bring in Article model
let Article = require('../models/article');
//user model
let User = require('../models/user');


// add route
router.get('/add', ensureAuthenticated, function(req,res){
	res.render('add_article',{
		title:'Add Article'
	});
});

// videoconference route
router.get('/videoconference', ensureAuthenticated, function(req,res){
	res.render('videoconference',{
		
	});
});

// chat route
router.get('/chat', ensureAuthenticated, function(req,res){
	res.render('chat',{
		
	});
});


//access control
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('danger', 'Please Login');
		res.redirect('/users/login');
	}
}


module.exports = router;
