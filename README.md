***Read Me******

*** CSCE 619 Advanced Topics in Computer Science Project ****

Functionality Done:
    Video and Text chat

Technologies used: 
    "connect-flash": "^0.1.1",
    "express": "^4.16.2",
    "express-messages": "^1.0.1",
    "express-session": "^1.15.6",
    "express-validator": "^4.3.0",
    "pug": "^2.0.0-rc.4"

To Run Application:
    1.	Get started with downloading the project from https://bitbucket.org/shivanjalikhare/csce_619/overview
    2.	Install the frameworks that are specified in the technologies used section.
    3.	After setting up the required dependencies, start the mongo DB by using      $ mongod --bind_ip=$IP –nojournal. Leave this running throughout the usage of the application.
    4.	Run app.js 

